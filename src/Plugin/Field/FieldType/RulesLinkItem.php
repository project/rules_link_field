<?php

namespace Drupal\rules_link_field\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'rules_link' field type.
 *
 * @FieldType(
 *   id = "rules_link",
 *   label = @Translation("Rules link"),
 *   description = @Translation("Link that triggers the Rules event."),
 *   default_widget = "rules_link",
 *   default_formatter = "rules_link",
 * )
 */
class RulesLinkItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['title'] = DataDefinition::create('string')
      ->setLabel(t('Link text'));

    $properties['classes'] = DataDefinition::create('string')
      ->setLabel(t('Link classes'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'title' => [
          'description' => 'Link text.',
          'type' => 'varchar',
          'length' => 255,
        ],
        'classes' => [
          'description' => 'Link classes.',
          'type' => 'varchar',
          'length' => 255,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('title')->getValue();
    return $value === NULL || $value === '';
  }

}
