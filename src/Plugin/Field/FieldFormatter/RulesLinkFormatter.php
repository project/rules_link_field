<?php

namespace Drupal\rules_link_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Component\Utility\Html;

/**
 * Plugin implementation of the 'rules_link' formatter.
 *
 * @FieldFormatter(
 *   id = "rules_link",
 *   label = @Translation("Rules link"),
 *   field_types = {
 *     "rules_link"
 *   }
 * )
 */
class RulesLinkFormatter extends FormatterBase {

  /**
   * The current user account.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('current_user')
    );
  }

  /**
   * Constructs a new RulesLinkFormatter.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Third party settings.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user account service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, AccountProxyInterface $current_user) {

    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t('Link that triggers the Rules event.');
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    // Do not show the link if a user doesn't have the appropriate permission.
    if (!$this->currentUser->hasPermission('execute rules that react on rules link clicked event')) {
      return [];
    }

    $elements = [];

    $entity = $items->getEntity();
    $entity_type = $entity->getEntityTypeId();
    $entity_id = $entity->id();
    $url = "/dispatch-rules-link-click-event?entity_type={$entity_type}&entity_id={$entity_id}";

    $id = $this->fieldDefinition->id();
    $id = str_replace(['.', '_'], '-', $id);

    foreach ($items as $delta => $item) {
      $id .= '-' . $delta;
      $elements[$delta] = [
        '#theme' => 'rules_link_field',
        '#url' => $url . "&link_id={$id}",
        '#id' => $id,
        '#class' => Html::escape($item->classes),
        '#title' => Html::escape($item->title),
      ];
    }

    return $elements;
  }

}
