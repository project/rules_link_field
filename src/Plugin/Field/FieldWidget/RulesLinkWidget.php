<?php

namespace Drupal\rules_link_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'rules_link' widget.
 *
 * @FieldWidget(
 *   id = "rules_link",
 *   module = "rules_link_field",
 *   label = @Translation("Rules link"),
 *   field_types = {
 *     "rules_link"
 *   }
 * )
 */
class RulesLinkWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $element['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Rules link text'),
      '#default_value' => $items[$delta]->title ?? NULL,
    ];

    $element['classes'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Rules link classes'),
      '#description' => $this->t('Classes separated by spaces, for instance: @class', ['@class' => 'button']),
      '#default_value' => $items[$delta]->classes ?? NULL,
    ];

    return $element;
  }

}
