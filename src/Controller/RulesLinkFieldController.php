<?php

namespace Drupal\rules_link_field\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\rules\Event\EntityEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Dispatches 'rules_link_click' event when a user click on the rules link.
 */
class RulesLinkFieldController extends ControllerBase {

  /**
   * Event dispatcher service.
   *
   * @var \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher
   */
  protected $eventDispatcher;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->eventDispatcher = $container->get('event_dispatcher');
    return $instance;
  }

  /**
   * Dispatch rules_link_click event.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request object.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   RedirectResponse object.
   */
  public function dispatchRulesLinkClickEvent(Request $request) {

    $entity_type = $request->query->get('entity_type');
    $entity_id = $request->query->get('entity_id');
    $link_id = $request->query->get('link_id');
    $entity = $this->entityTypeManager()->getStorage($entity_type)->load($entity_id);
    $entity_type_id = $entity->getEntityTypeId();

    $event = new EntityEvent($entity, [
      $entity_type_id => $entity,
      'link_id' => $link_id,
    ]);

    $this->eventDispatcher->dispatch("rules_link_click:$entity_type_id", $event);

    // Redirect a user back to the page where the link was clicked.
    $destination = $request->headers->get('referer');
    return new RedirectResponse($destination);
  }

}
